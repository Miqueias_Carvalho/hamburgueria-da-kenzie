import "./style.css";
function Product({ id, name, category, price, handleClick }) {
  return (
    <div className="container_info">
      <div className="info">
        <h2>{name}</h2>
        <p>{category}</p>
        <p>
          {price.toLocaleString("pt-BR", {
            style: "currency",
            currency: "BRL",
            minimumFractionDigits: 2,
            maximumFractionDigits: 2,
          })}
        </p>
      </div>
      <div>
        <button onClick={() => handleClick(id)}>Adicionar</button>
      </div>
    </div>
  );
}
export default Product;
