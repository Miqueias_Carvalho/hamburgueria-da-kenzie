import "./style.css";

function Carrinho({ exibirCarrinho, removerItemDoCarrinho, carrinho }) {
  return (
    <div className="modal-pai">
      <div className="modal">
        <table className="tabela">
          <caption>Você selecionou {carrinho.total} itens</caption>
          <thead className="tabela_cabeçalho">
            <tr>
              <th className="coluna_Id">Id</th>
              <th className="coluna_detalhes">Detalhes</th>
              <th className="coluna_price">Preço</th>
            </tr>
          </thead>
          <tbody className="tabela_corpo">
            {carrinho.saleDetails.map((item, index) => (
              <tr key={`carrinho_id${index}`}>
                <td className="coluna_Id">{item.id}</td>
                <td className="coluna_detalhes">
                  <p className="detalhes_item-principal">{item.name}</p>
                  <p className="detalhes_item-categoria">{item.category}</p>
                </td>

                <td className="coluna_price">{item.price}</td>
                <td>
                  <button
                    className="tabela_botão-remover"
                    onClick={() => removerItemDoCarrinho(item.id)}
                  >
                    -
                  </button>
                </td>
              </tr>
            ))}
            <tr></tr>
          </tbody>
        </table>
        <div className="botao-fechar">
          <button onClick={exibirCarrinho}>Fechar</button>
        </div>
      </div>
    </div>
  );
}
export default Carrinho;
