import Product from "../Product";
import "./style.css";

function MenuContainer({ products, handleClick }) {
  return (
    <div className="menucontainer">
      {products.map((elem, index) => (
        <Product
          key={index}
          id={elem.id}
          name={elem.name}
          category={elem.category}
          price={elem.price}
          handleClick={handleClick}
        />
      ))}
    </div>
  );
}
export default MenuContainer;
