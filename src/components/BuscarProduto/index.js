import { useState } from "react";
import "./style.css";

function BuscarProduto({ showProducts }) {
  const [item, setItem] = useState("");

  const pesquisar = () => {
    showProducts(item);
    setItem("");
  };

  return (
    <div className="container_busca">
      <div>
        <input
          type="text"
          value={item}
          placeholder="Digite o nome do produto"
          onChange={(e) => setItem(e.target.value)}
        ></input>
      </div>
      <div>
        <button onClick={pesquisar}>Pesquisar</button>
      </div>
    </div>
  );
}
export default BuscarProduto;
