import "./App.css";
import { GiShoppingCart, GiHamburger } from "react-icons/gi";
import { useState } from "react";
import MenuContainer from "./components/MenuContainer";
import BuscarProduto from "./components/BuscarProduto";
import Carrinho from "./components/Carrinho";

function App() {
  // EStados---------------------------------------------------------------
  const estadoInicial = [
    { id: 1, name: "Hamburguer", category: "Sanduíches", price: 7.99 },
    { id: 2, name: "X-Burguer", category: "Sanduíches", price: 8.99 },
    { id: 3, name: "X-Salada", category: "Sanduíches", price: 10.99 },
    { id: 4, name: "Big Kenzie", category: "Sanduíches", price: 16.99 },
    { id: 5, name: "Guaraná", category: "Bebidas", price: 4.99 },
    { id: 6, name: "Coca", category: "Bebidas", price: 4.99 },
    { id: 7, name: "Fanta", category: "Bebidas", price: 4.99 },
  ];
  const [products, setProducts] = useState(estadoInicial);

  const [filteredProducts, setFilteredProducts] = useState([]);

  const [currentSale, setCurrentSale] = useState({ total: 0, saleDetails: [] });

  const [abrirCarro, setAbrirCarro] = useState(false);

  //Funções------------------------------------------------------------------
  const showProducts = (buscado) => {
    let pesquisados = products.filter((item) => {
      return (
        item.name.toLocaleLowerCase().includes(buscado.toLocaleLowerCase()) ||
        item.category.toLocaleLowerCase().includes(buscado.toLocaleLowerCase())
      );
    });
    setFilteredProducts(pesquisados);
  };

  const handleClick = (idProduto) => {
    let produtoSelecionado = products.find((item) => item.id === idProduto);
    let quantidadeDeItens = currentSale.saleDetails.length + 1;

    let duplicou = currentSale.saleDetails.includes(produtoSelecionado);
    if (!duplicou) {
      setCurrentSale({
        total: quantidadeDeItens,
        saleDetails: [...currentSale.saleDetails, produtoSelecionado],
      });
    }
  };

  const ShowSale = () => {
    const listaCarrinho = currentSale.saleDetails;

    return listaCarrinho.map((item, index) => (
      <div className="showsale_carrinho" key={`produto_${index}`}>
        <h2>{item.name}</h2>
        <p>Categoria - {item.category}</p>
        <p>Preço - {item.price} R$</p>
      </div>
    ));
  };
  const exibirCarrinho = () => {
    setAbrirCarro(!abrirCarro);
    // console.log(abrirCarro);
    // let exibir = document.getElementById("modal-pai");
    // exibir.classList.toggle("hidden");
  };
  const removerItemDoCarrinho = (itemId) => {
    // let itensMantidos = currentSale.saleDetails.filter(
    //   (produto) => produto.id !== itemId
    // );
    // setCurrentSale({ total: itensMantidos.length, saleDetails: itensMantidos });

    let indiceDoItemRemovido = -1;
    for (let i = 0; i < currentSale.saleDetails.length; i++) {
      if (currentSale.saleDetails[i].id === itemId) {
        indiceDoItemRemovido = i;
        break;
      }
    }

    if (indiceDoItemRemovido >= 0) {
      let itensMantidos = [
        ...currentSale.saleDetails.slice(0, indiceDoItemRemovido),
        ...currentSale.saleDetails.slice(indiceDoItemRemovido + 1),
      ];
      setCurrentSale({
        total: itensMantidos.length,
        saleDetails: itensMantidos,
      });
    }
  };
  //JSX-----------------------------------------------------------------------------
  return (
    <div className="main_container">
      <header className="cabeçalho">
        KZ-Burguer
        <GiHamburger size="90px" color="#ceb988" />
      </header>
      <div className="App">
        <BuscarProduto showProducts={showProducts} />
        <MenuContainer
          products={filteredProducts.length > 0 ? filteredProducts : products}
          handleClick={handleClick}
        ></MenuContainer>

        <div className="subtotal">
          <div>
            Subtotal -{" "}
            <span>
              {currentSale.saleDetails
                .reduce((vAnt, vAtual) => vAnt + vAtual.price, 0)
                .toLocaleString("pt-BR", {
                  style: "currency",
                  currency: "BRL",
                  minimumFractionDigits: 2,
                  maximumFractionDigits: 2,
                })}
            </span>
          </div>
          <div>
            <button onClick={exibirCarrinho}>
              <GiShoppingCart size="22px" />
            </button>
          </div>
        </div>
        {currentSale.total > 0 && (
          <div className="container_showsale">{ShowSale()}</div>
        )}
      </div>
      {abrirCarro && (
        <Carrinho
          exibirCarrinho={exibirCarrinho}
          removerItemDoCarrinho={removerItemDoCarrinho}
          carrinho={currentSale}
        />
      )}
    </div>
  );
}

export default App;
